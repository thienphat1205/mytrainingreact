import React, {Component} from 'react';
import './styles.scss';

class BodyProflie extends Component{
	render(){
		return(
				<div >
					<div className="avatar">
		    			<div className="image-profile">
		    				<img src="https://cdn2.iconfinder.com/data/icons/rcons-user/32/male-circle-512.png" />
		    			</div>
		    			<div id="change-icon">
	                   		<a href="">
	                      		 <img id="change-ava" alt="" src="https://www.materialui.co/materialIcons/image/edit_grey_192x192.png"/>        
	                    	</a>
	                	</div>
	                </div>
	    			<div className="row form-profile">
	    				<form action="#" method="Post">
	    					<div className="content-form">
	    						<div className="col-sm-6 form-left">
			    					<p className="text-form-profile">Display Name</p>						    	
							    	<input className="form" type="text" placeholder="Your Name" name="displayname" required />						    	
							    	<p className="text-form-profile">Email</p>						    	
							    	<input className="form" type="email" placeholder="Your Company Email" name="email" required />		
							    	<p className="text-form-profile">New Password</p>						  
							    	<input className="form" type="password" placeholder="Enter New Password" name="psw" required />
							    	
			    				</div>
			    				<div className="col-sm-6 form-right">
			    					<p className="text-form-profile">Display Info</p>						    	
							    	<input className="form" type="text" placeholder="Your Info" name="info" required />						    	
							    	<p className="text-form-profile">Phone Number</p>						    	
							    	<input className="form" type="text" placeholder="Phone" name="phone" required />	
			    				    <p className="text-form-profile">Confirm Password</p>						  
							    	<input className="form" type="password" placeholder="Confirm Password" name="confirmpsw" required />
			    				</div>
	    					</div>
	    					<div className="button-update">
						    		<button className="update" type="submit">UPDATE PROFILE</button>
						    </div>	

	    				</form>
	    			</div>
	    		</div>
				
			);
			}
}
export default BodyProflie;
	   			    				/*https://cdn2.iconfinder.com/data/icons/rcons-user/32/male-circle-512.png*/