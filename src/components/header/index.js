import React, {Component} from 'react';
import './styles.scss';

class Header extends Component{
	render(){
		return(
			<div>
				<header>					
	    			<div className="row">
	    				<div className="col-sm-2 logo-header">
	    					<img src="https://www.terralogic.com/img/terra-logo.png"/>
	    				</div>
	    				<div className="col-sm-8">  				
	    				</div>
	    				<div className="col-sm-2 text-header">PROFILE
	    		   		
	    				</div>
	    			</div>    
    		
				</header>
	    		
    		</div>
				
			);
			}
}
export default Header;