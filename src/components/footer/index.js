import React, {Component} from 'react';
import './styles.scss';

class Footer extends Component{
	render(){
		return(
			<div>
				<footer>					
	    			    <div className="text-footer">
	    					<p>© 2019 Terralogic, Inc</p>
	    				</div>	
				</footer>
	    		
    		</div>
				
			);
			}
}
export default Footer;