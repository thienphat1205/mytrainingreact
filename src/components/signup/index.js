import React, {Component} from 'react';
import './styles.scss';

class SignupRight extends Component{
	render(){
		return(
				 <div className="row">
    				<div className="box-signup">
    					<div className="text-login">CREATE ACCOUNT
    					</div>
    					<div className="login-form">
						    <form action="#" method="Post">
						    	<p className="text-form">Email</p>						    	
						    	<input className="email" type="text" placeholder="Your Company Email" name="email" required />						    	
						    	<p className="text-form">Password</p>						  
						    	<input className="password" type="password" placeholder="Your Password" name="psw" required />
						    	<p className="text-form">Confirm Password</p>						  
						    	<input className="password" type="password" placeholder="Confirm Password" name="confirm" required />
						    	<div className="row">
    									<div className="col-sm-4">			
						    			</div>	
						    			<div className="col-sm-4 button-confirm ">
						    				<button className="confirm" type="submit">CONFIRM</button>
						    			</div>
						    			<div className="col-sm-4">						    				
						    			</div>	
						    	</div>						    	
						    </form>
						</div>
    				</div> 
    			</div> 
			);
			}
}
export default SignupRight;