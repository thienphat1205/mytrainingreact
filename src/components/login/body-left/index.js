import React, {Component} from 'react';
import './styles.scss';

class Left extends Component{
	render(){
		return(
				<div className="row">
	    				<div className="logo">
	    				 	<img src="https://www.terralogic.com/img/terra-logo.png"/>
	    				</div>
	    				<div className="text1">
	    					<p>We Are Family</p>
	    				</div>
	    				<div className="text2">
	    					<p>© 2019 Terralogic, Inc</p>
	    				</div>
    			</div>
    			
			);
			}
}
export default Left;