import React, {Component} from 'react';
import './styles.scss';

class Right extends Component{
	render(){
		return(
				 <div className="row">
    				<div className="box-login">
    					<div className="text-login">LOGIN
    					</div>
    					<div className="login-form">
						    <form action="#" method="Post">
						    	<p className="text-form-login">Email</p>						    	
						    	<input className="email-login" type="text" placeholder="Your Company Email" name="email" required />						    	
						    	<p className="text-form-login">Password</p>						  
						    	<input className="password-login" type="password" placeholder="Your Password" name="psw" required />
						    	<div className="row">
    									<div className="col-sm-4 button-login">
						    				<button className="login" type="submit">LOGIN</button>
						    			</div>	
						    			<div className="col-sm-4"></div>
						    			<div className="col-sm-4 button-signup">
						    				<button className="signup" type="submit">SIGN UP</button>
						    			</div>	
						    	</div>						    	
						    </form>
						    <div className="text-responsive">
	    					<p>© 2019 Terralogic, Inc</p>
	    				</div>
						</div>
    				</div> 
    			</div> 
			);
			}
}
export default Right;