import React, {Component} from 'react';
import './styles.scss';
import Left from '../../components/login/body-left';
import Right from '../../components/login/body-right';

class Login extends Component{
	render(){
		return(
			<div className="container">
	    		<div className="row body-login">
	    			<div className="col-sm-6 body-left">
	    				<Left />
	    			</div>
	    			<div className="col-sm-6 body-right">
	    		   		<Right />
	    			</div>
	    		</div>    
    		</div>
			);
			}
}
export default Login;