import React, {Component} from 'react';
import Left from '../../components/login/body-left';
import SignupRight from '../../components/signup';

class Signup extends Component{
	render(){
		return(
			<div className="container">
	    		<div className="row">
	    			<div className="col-sm-6 body-left">
	    				<Left />
	    			</div>
	    			<div className="col-sm-6 body-right">
	    		   		<SignupRight />
	    			</div>
	    		</div>
    
    		</div>
			);
			}
}
export default Signup;