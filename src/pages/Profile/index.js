import React, {Component} from 'react';
import './styles.scss';
import Header from '../../components/header';
import Footer from '../../components/footer';
import BodyProfile from '../../components/profile';

class Profile extends Component{
	render(){
		return(	
				<div>		  
	    			<div> <Header/> </div>
	    			<div className="profile">
	    				<div className="row">	    				 	
    						<div className="col-sm-3"></div>	
						    <div className="col-sm-6 body-profile">
						    	<BodyProfile />
						    </div>
						    <div className="col-sm-3"></div>	
						</div>
	    			</div>	    			
	    			<div> <Footer/> </div>
	    		</div>
			);
			}
}
export default Profile;